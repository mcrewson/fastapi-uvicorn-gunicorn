FROM registry.gitlab.com/mcrewson/uvicorn-gunicorn:python3.9

LABEL maintainer="Mark Crewson <mark@crewson.net>"

RUN pip install --no-cache-dir fastapi

COPY ./app /app
