#!/usr/bin/env bash
set -e

use_tag="registry.gitlab.com/mcrewson/fastapi-uvicorn-gunicorn:$NAME"

DOCKERFILE="$NAME"

if [ "$NAME" == "latest" ] ; then
    DOCKERFILE="python3.9"
fi

docker build -t "$use_tag" --file "./docker/${DOCKERFILE}.dockerfile" "./docker/"
